h1. Getting Started

h2. 5-Minutes Tutorial

*Please note:* Due to issue "#127":http://code.google.com/a/eclipselabs.org/p/spray/issues/detail?id=127 it is currently not possible to use Spray via update site. We hope to fix this soon. As work around you have to start Spray out of the development workspace.

h3. Create a new EMF project

First we have to create or reuse a meta model of a business domain that can be referenced by the diagram model to be created in the second step.

* File -> New -> Other...

!images/empty_emf_project.png!

* Project name: org.mod4j.dsl.businessdomain.mm
* Download "BusinessDomainDsl.ecore":http://spray.eclipselabs.org.codespot.com/git/examples/one/org.mod4j.dsl.businessdomain.mm/model/BusinessDomainDsl.ecore and "BusinessDomainDsl.genmodel":http://spray.eclipselabs.org.codespot.com/git/examples/one/org.mod4j.dsl.businessdomain.mm/model/BusinessDomainDsl.genmodel
* put the downloaded files in the model folder of the just created EMF project
* Download "plugin.xml":http://spray.eclipselabs.org.codespot.com/git/examples/one/org.mod4j.dsl.businessdomain.mm/plugin.xml and put it directly in the EMF project
* Double click on the BusinessDomainDsl.genmodel and open the context menu at the root element

!images/GenModelGenerate.png!

* Use the quick fix to solve the problems in the MANIFEST.MF (sets the singleton directive) 

h3. Create a new project

* Create a new Spray project by using the Spray Project Wizard. Open _File / New / Project / Spray / Spray Project_.

!images/projectwizard_1.png!

* Enter the project name @org.eclipselabs.spray.examples.busmod@.
* On the first page you will have to select a EMF metamodel to work with. Click the _Browse Workspace_ button to select an EMF Ecore file from your workspace.

!images/projectwizard_2b.png!

* Spray requires that it can find the corresponding genmodel for the EPackage. If it can be found next to the Ecore file it will be added automatically.

!images/projectwizard_3b.png!

* Press _Next_ (or _Finish_ to skip the next page)
* The next page lets you modify some settings for Spray's code generator, like package names to use. Leave the defaults and press _Finish_ for now.

!images/projectwizard_4.png!

h3. Define Spray Model

After finishing the project wizard the Spray model editor opens. Paste this model into file @busmod.spray@:

bc.. 
import BusinessDomainDsl.*

diagram busmod for BusinessDomainDsl.BusinessDomainModel

class BusinessClass {
    container  ( fill=RGB(217,228,255) ) 
    {
          text ( )  { "<<"+eClass.name+">> " + name};  
          line ( color=black width=2);
          reference properties attribute dataType;
          line (width=1 color=blue);
          reference businessRules;   // will use name property by default
          line (width=1 color=blue);
          
          text () "::" + description;    
    }
    references {
         superclass : connection(); 
    }
    behavior {
        create into types palette "Shapes";
    }
 }
 
class Association {  
    connection () 
    {
        from source;
        to   target;
        toText text() targetMultiplicity.name;
    } 
    behavior {
        create into associations palette "Connections" ;
    }
}
p. 
After saving the model the editor sources will be automatically generated to the @src-gen@ folder of the project.
!images/gettingstarted_1.png!

p. 
Replace the plugin.xml in the project root (in the beginning there is even no plugin.xml in the project root) with the plugin.xml from src-gen. 

h3. Test the editor

* Start a Runtime Eclipse instance with _Run As / Eclipse Application_. A new Eclipse instance starts with the plugins deployed.

* Create a new project. The type of project does not matter, but for simplicity choose new Java Project and name it "BusmodTest".
* Right-click on the @src@ folder, choose _File / New / Other -> Examples / Graphiti / Graphiti Diagram_
!images/gettingstarted_2.png!
* Choose the Diagram Type "busmod"
!images/gettingstarted_3.png!
* Your new diagram editor opens!
!images/gettingstarted_4.png!

* as alternative you can create your diagram also this way:
** Right-click on the @src@ folder, choose _File / New / Other -> busmod / New busmod Diagram_
!images/gettingstarted_2b.png!
** Give it a name
!images/gettingstarted_3b.png!
** in difference to the wizard used above there are now created two files
*** newDiagram.businessdomaindsl: this hold the domain model
*** newDiagram.diagram: this hold the pure diagram related stuff and just references elements from the newDiagram.businessdomaindsl

h3. Create your first element

* Click on _Business class_ in the palette to select that entry
* Click somewhere in the diagram editor to create the element
* _Window -> Show View -> Other... -> General/Properties to open the properties view
* Click on the just created element and you can see its attribute values in the properties view
* Type _Person_ in the name field
* Resize the box in the diagram editor (move on the right side of the box and when the horizontal double arrow appears hold the left mouse button and move to the right) to see that the change has been applied

h2. 15-Minutes Tutorial

In this section we will extend the example from the 5-Minutes Tutorial further. Follow the steps mentioned above first.

h3. Implement a custom behavior

Extend the busmod.spray model like this:

bc.. 

diagram busmod for BusinessDomainDsl.BusinessDomainModel

behavior actions { 
  custom showInfoForBusinessClass "Show Information"
}


class BusinessClass {
    ...
    behavior {
		  create into types palette "Shapes";
        group actions    
    }
    ...
p. 

Find the @BusmodCustomShowInfoForBusinessClassFeature.java@ in your workspace by pressing _CTRL + T_

!images/open_custom_feature.png!

Having the _Link with Editor_ toggle button enabled you see the corresponding file in the package explorer when you open the found Java class file.

Right-click on @BusmodCustomShowInfoForBusinessClassFeature@ from the @features@ package. Select _Spray -> Move to source folder for manual extension_. 

!images/move_to_src.png!

The file is moved from @src-gen@ to @src@ and opens in the editor.

Implement the execute() method to open a message dialog.

bc.. 
    @Override
    public void execute(ICustomContext context, EObject object) {
        MessageDialog.openInformation(Display.getDefault().getActiveShell(), "Information", 
                "Selected object of type "+object.eClass().getName());
    }
    
!images/adapted_feature_class.png!

p. 

After restarting you can invoke an action "Show Information" from the context menu of a BusinessClass shape.

!images/gettingstarted_5.png!

!images/gettingstarted_6.png!

h2. 20-Minutes Tutorial

In this section we will show you how to integrate the Spray DSL with the Shape and Style DSL. This tutorial can be applied after the 5-Minutes tutorial.

As you may have already noticed there are three files:
* busmod.style
* busmod.shape
* busmod.spray

In the 5-Minutes tutorial we just used the Spray DSL built-in capabilities for defining shapes (e.g. rectangle, line) and their styling (e.g. yellow background).

But it is possible to modularize those aspects out. Shapes can be defined in files with the file extension _.shape_ and styles can be defined in files with the file extension _.style_. Shape and style definition can then be imported and reused.

In the followong for the business model DSL this modularization is shown by example. For a full reference of the capabilities of Shape and Style DSL see the later sections of this user guide.

h3. busmod.style

There is already a predefined style BusmodDefaultStyle contained in the busmod.style file. You can reuse styles by extending them as done here for the BusinessClassStyle. This style will also use e.g. 12pt as font-size but additional defines the background color to be yellow. 

bc.. 
style BusmodDefaultStyle {
    description = "The default style of the bm diagram type."
    // transparency = 0.95
    // background-color = black
    // line-color = black
    // line-style = solid
    // line-width = 1
    // font-color = black
    // font-name = "Tahoma"
    font-size = 12
    // font-bold = yes
}

style BusinessClassStyle extends org.eclipselabs.spray.styles.BusmodDefaultStyle {
    description = "The style for the business class."
    background-color = yellow
}

h3. busmod.shape

A shape is constructed by graphical elements. The element's positions/coordinates and dimensions are defined by absolute values (absolute within the shape container, not within the hole diagram). In the shape file you can reference to styles defined in a style file (done for the rectangle in this case) or define styles within the shape file (used for line). 

Now look at the text element. The label of the text is parametrized: the parameter _businessClassText_ is assigned to _id_. The value of _businessClassText_ have to be bound in the usage context of the shape. We will see this in busmod.spay file.  

bc.. 
shape BusinessClassShape  {
    rectangle style org.eclipselabs.spray.styles.BusinessClassStyle {
        position(x=0,y=0)
        size(width=100,height=100)
    }
    text { 
        position(x=5,y=5) 
        size(width=90,height=50)
        id = businessClassText
    }
    line { 
        point(x=0, y=51) 
        point(x=100, y=51)
        style (
            line-color=black
            line-width=2
        )  
    }
 }

h3. busmod.spray

As you can see a style definition may not only be used in the shape file but also directly in the spray file. By referencing the _BusmodDefaultStyle_ at diagram level it will be applied to all shape and connection elements in this diagram. 

In the class BusinessClass the shape is referenced and the parameter _businessClassText_ is bound to the value of the domain model element _BusinessClass_' attribute _name_.

In contrast to the 5-Minutes tutorial we now use the askFor property. This will trigger an input dialog when you create a box or a connection in the diagram editor asking for the name of the box resp. connection. This value is then assigned immediately to the name attribute in this case of _BusinessClass_ resp. _Association_.      

bc.. 
import BusinessDomainDsl.*

diagram busmod for BusinessDomainModel style BusmodDefaultStyle

class BusinessClass icon "ecore/EClass.gif" {
	shape org.eclipselabs.spray.shapes.BusinessClassShape {
		name into businessClassText
	}
	behavior {
		create into types palette "Shapes" askFor name;
	}
}

class Association icon "connection16.gif" {
	connection ( ) {
		from source;
		to target;
		fromText text ( ) {
			source.name
		}
	}
	behavior {
		create into associations palette "Connections" askFor name
	}
}


