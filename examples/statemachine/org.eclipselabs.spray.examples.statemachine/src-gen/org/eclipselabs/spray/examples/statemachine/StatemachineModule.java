/*************************************************************************************
 *
 * Generated on Sun Mar 25 15:23:33 CEST 2012 by Spray GuiceModule.xtend
 * 
 * This file is an extension point: copy to "src" folder to manually add code to this
 * extension point.
 *
 *************************************************************************************/
package org.eclipselabs.spray.examples.statemachine;

public class StatemachineModule extends StatemachineModuleBase {
     // Add custom bindings here
     // public Class<? extends MyInterface> bindMyInterface () {
     //   return MyInterfaceImpl.class;
     // }
     //
     // Get instances through the Activator:
     // MyInterface instance = Activator.get(MyInterface.class);
}
