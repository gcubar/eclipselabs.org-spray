/*************************************************************************************
 *
 * Generated on Sun Mar 25 15:23:33 CEST 2012 by Spray UpdateConnectionFromDslFeature.xtend
 * 
 * This file is an extension point: copy to "src" folder to manually add code to this
 * extension point.
 *
 *************************************************************************************/
package org.eclipselabs.spray.examples.statemachine.features;

import org.eclipse.graphiti.features.IFeatureProvider;

public class StatemachineUpdateUebergangFeature extends StatemachineUpdateUebergangFeatureBase {
    public StatemachineUpdateUebergangFeature(IFeatureProvider fp) {
        super(fp);
    }
}
