/*************************************************************************************
 *
 * Generated on Sun Mar 25 15:23:33 CEST 2012 by Spray ToolBehaviorProvider.xtend
 * 
 * This file is an extension point: copy to "src" folder to manually add code to this
 * extension point.
 *
 *************************************************************************************/
package org.eclipselabs.spray.examples.statemachine.diagram;

import org.eclipse.graphiti.dt.IDiagramTypeProvider;
 
public class StatemachineToolBehaviorProvider extends StatemachineToolBehaviorProviderBase {
    public StatemachineToolBehaviorProvider(IDiagramTypeProvider dtp) {
        super(dtp);
    }
}
