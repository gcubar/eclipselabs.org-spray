/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package BusinessDomainDsl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Business Rule</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see BusinessDomainDsl.IBusinessDomainDslPackage#getBusinessRule()
 * @model
 * @generated
 */
public interface IBusinessRule extends IAbstractBusinessRule {
} // IBusinessRule
