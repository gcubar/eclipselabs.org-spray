/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package BusinessDomainDsl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Blob Binary Property</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see BusinessDomainDsl.IBusinessDomainDslPackage#getBlobBinaryProperty()
 * @model
 * @generated
 */
public interface IBlobBinaryProperty extends IProperty {
} // IBlobBinaryProperty
