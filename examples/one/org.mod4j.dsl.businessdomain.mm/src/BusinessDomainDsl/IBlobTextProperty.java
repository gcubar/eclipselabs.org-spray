/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package BusinessDomainDsl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Blob Text Property</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see BusinessDomainDsl.IBusinessDomainDslPackage#getBlobTextProperty()
 * @model
 * @generated
 */
public interface IBlobTextProperty extends IProperty {
} // IBlobTextProperty
