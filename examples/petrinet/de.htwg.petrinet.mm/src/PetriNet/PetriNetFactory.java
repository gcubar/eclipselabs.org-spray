/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package PetriNet;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see PetriNet.PetriNetPackage
 * @generated
 */
public interface PetriNetFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	PetriNetFactory eINSTANCE = PetriNet.impl.PetriNetFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Petri Node</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Petri Node</em>'.
	 * @generated
	 */
	PetriNode createPetriNode();

	/**
	 * Returns a new object of class '<em>Place EClass</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Place EClass</em>'.
	 * @generated
	 */
	PlaceEClass createPlaceEClass();

	/**
	 * Returns a new object of class '<em>Token EClass</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Token EClass</em>'.
	 * @generated
	 */
	TokenEClass createTokenEClass();

	/**
	 * Returns a new object of class '<em>Transition EClass</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Transition EClass</em>'.
	 * @generated
	 */
	TransitionEClass createTransitionEClass();

	/**
	 * Returns a new object of class '<em>Arc EClass</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Arc EClass</em>'.
	 * @generated
	 */
	ArcEClass createArcEClass();

	/**
	 * Returns a new object of class '<em>Petri Model</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Petri Model</em>'.
	 * @generated
	 */
	PetriModel createPetriModel();

	/**
	 * Returns a new object of class '<em>Petri Edge</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Petri Edge</em>'.
	 * @generated
	 */
	PetriEdge createPetriEdge();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	PetriNetPackage getPetriNetPackage();

} //PetriNetFactory
