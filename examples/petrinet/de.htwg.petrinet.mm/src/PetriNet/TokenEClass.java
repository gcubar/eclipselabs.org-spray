/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package PetriNet;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Token EClass</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see PetriNet.PetriNetPackage#getTokenEClass()
 * @model
 * @generated
 */
public interface TokenEClass extends PetriNode {
} // TokenEClass
