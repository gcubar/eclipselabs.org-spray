/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package PetriNet;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Transition EClass</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see PetriNet.PetriNetPackage#getTransitionEClass()
 * @model
 * @generated
 */
public interface TransitionEClass extends PetriNode {
} // TransitionEClass
