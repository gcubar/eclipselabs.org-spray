/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package PetriNet;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Petri Edge</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see PetriNet.PetriNetPackage#getPetriEdge()
 * @model
 * @generated
 */
public interface PetriEdge extends PetriModel {
} // PetriEdge
