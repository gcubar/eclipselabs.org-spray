/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package PetriNet;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Petri Node</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see PetriNet.PetriNetPackage#getPetriNode()
 * @model
 * @generated
 */
public interface PetriNode extends PetriModel {
} // PetriNode
