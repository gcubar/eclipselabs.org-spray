package org.eclipselabs.spray.runtime.graphiti.styles;

import org.eclipse.graphiti.mm.algorithms.styles.GradientColoredAreas;

public interface ISprayGradient {

	public GradientColoredAreas getGradientColoredAreas( );
	
}
